# Rootless podman container image to run rootless GitLab Runner service.
# Copyright (C) 2023 Christopher C. Evich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program

FROM quay.io/centos/centos:stream9

ADD /root/ /root/
ADD /etc/ /etc/
ADD /home/ /home/

# Allow image-builders to choose another version besides "latest" should
# an incompatible change be introduced.
ARG RUNNER_VERSION=latest

# Permit building containers for alternate architectures. At the time
# of this commit, only 'arm64' is available.
ARG TARGETARCH=amd64

# Allow image-builders to choose an alternate nested-container pruning cycle.
# For most people the default is probably fine.  This setting is dependent
# on the number and frequency of jobs run, along with the amount of disk-space
# available for both /cache and /home/podman/.local/share/containers volumes.
ARG PRUNE_INTERVAL=daily  # see systemd.timer for allowable values

# All-in-one packaging/image-setup script to keep things simple.
RUN PRUNE_INTERVAL=${PRUNE_INTERVAL} \
    RUNNER_VERSION=${RUNNER_VERSION} \
    bash /root/setup.sh

VOLUME /cache /home/podman/.local/share/containers
ENTRYPOINT /lib/systemd/systemd

# Gitlab-runner configuration options, may be freely overridden at
# container image build time.
ARG DEFAULT_JOB_IMAGE=registry.fedoraproject.org/fedora-minimal:latest

# Allow image-builders to override the Gitlab URL
ARG GITLAB_URL=https://gitlab.com/

# Run nested containers in --privileged mode - required to allow building
# container images using podman or buildah.  Otherwise may be set 'false'.
ARG NESTED_PRIVILEGED=true

# Download the FIPS version of gitlab-runner when enabled on the host system.
ARG ENABLE_FIPS=true

# The registration runlabel may be called multiple times to register more than
# one runner.  Each expects a REGISTRATION_TOKEN secret to be pre-defined and
# the file './config.toml' to exist (may be empty).  A local-cache volume
# '/cache' is configured for bind-mounting into all interrior-containers
# for container-runtime use, as recommended by the docs.  Other settings
# may be changed if you know what you're doing.
LABEL register="podman run -it --rm \
 --secret=REGISTRATION_TOKEN,type=env \
 -v ./config.toml:/etc/gitlab-runner/config.toml:Z \
 -e REGISTER_NON_INTERACTIVE=true \
 -e CI_SERVER_URL=${GITLAB_URL} \
 -e RUNNER_NAME=pipglr \
 -e RUNNER_EXECUTOR=docker \
 -e RUNNER_SHELL=bash \
 -e DOCKER_HOST=unix:///home/runner/podman.sock \
 -e DOCKER_IMAGE=${DEFAULT_JOB_IMAGE} \
 -e DOCKER_CACHE_DIR=/cache \
 -e DOCKER_VOLUMES=/cache \
 -e DOCKER_NETWORK_MODE=host \
 -e DOCKER_PRIVILEGED=${NESTED_PRIVILEGED} \
 --entrypoint=/usr/bin/gitlab-runner \$IMAGE register"

# Additionally, the nested-podman storage volumes must be pre-created with
# 'podman' UID/GID values to allow nested containers access.
LABEL setupstorage="podman volume create --opt o=uid=1000,gid=1000 pipglr-storage"

# Lastly, the gitlab-runner will manage container-cache in this directory,
# which will also be bind-mounted into every container.  So it must be
# writable by both 'podman' user and 'runner' group.
LABEL setupcache="podman volume create --opt o=uid=1000,gid=1001 pipglr-cache"

# Helper to extract the current configuration secret to allow editing.
LABEL dumpconfig="podman run -it --rm \
 --secret config.toml --entrypoint=/bin/cat \
 \$IMAGE /var/run/secrets/config.toml"

# Executing the runner container depends on the config.toml secret being
# set (see above) and two volumes existing with correct permissions set.
# Note: The contents of the volumes are not critical, they may be removed
# and re-created (see above) to quickly free-up disk space.
LABEL run="podman run -dt --name pipglr \
 --secret config.toml,uid=1001,gid=1001 \
 -v pipglr-storage:/home/podman/.local/share/containers \
 -v pipglr-cache:/cache \
 --systemd true --privileged \
 --device /dev/fuse \$IMAGE"
# ==========================
